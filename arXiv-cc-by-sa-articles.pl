#!/usr/bin/perl
# Get CC-BY-SA licensed articles from arXiv.org
# Author: Rajeesh K V <rajeeshknambiar@gmail.com>
# License: GNU GPLv3
# Usage: "$0 -y 2016 -m 05"
use utf8;
use strict; use warnings;
use HTML::TreeBuilder; use LWP::UserAgent;
use Getopt::Std;

my %options = ();
getopts("y:m:", \%options); # -y YEAR -m MONTH
my $yy;
my $mm;
if ( defined $options{y} ) {
    die "Please pass -y <year> in 4 digit format" unless length($options{y}) == 4 ;
    $yy = substr($options{y},2);    # Last 2-digits
}
else {
    $yy = substr( (localtime)[5] + 1900 , 2); #Last 2-digits
}
if ( defined $options{m} ) {
    die "Please pass -m <month> in 2 digit format" unless length($options{m}) == 2; 
    $mm = $options{m};    # Last 2-digits
}

# If month is given, take it. Otherwise, iterate all months 01...12
my @months;
if ( $mm ) {
    @months = ($mm);
}
else {
    my $i;
    for ( $i = 1; $i < 12; $i += 1) {
        $months[$i - 1] = sprintf("%02d",$i);   #2-digit month 01, 02, ... 12
    }
}

foreach (@months) {
    print "Checking High Energy Physics articles of year $yy month $_ : ";
    my $hept_url = "https://arxiv.org/list/hep-th/".$yy.$_."?show=600";    #High Energy Physics, YYMM, 600 records
    print "$hept_url \n";
#my $hept_url = "https://arxiv.org/list/hep-th/1612?show=600";    #High Energy Physics, YYMM, 600 records
#my $hept_url = "https://arxiv.org/list/hep-th/1612";    #High Energy Physics, YYMM, 25 records by default

    my $tree = HTML::TreeBuilder->new_from_url($hept_url);  #Gets the list of articles
#my $listcont = $tree->as_text();
#print "$listcont \n";
# Look for 25 items in the list
    my $art_url;
#foreach $art_url ( $tree->look_down( _tag => 'dt', 'class' => 'list-identifier') ){
    foreach $art_url ( $tree->look_down( _tag => 'dt') ){
        if ( $art_url->as_XML =~ m|\[(\d+)\]| ) {
            print "Checking license: $1 \n";
        }
        $art_url = $art_url->look_down( _tag => 'span', 'class' => 'list-identifier');
        $art_url = $art_url->look_down( _tag => 'a');
        $art_url = $art_url->as_text;
        #print "art_url : $art_url \n";
        if ( $art_url =~ m|([0-9]+.[0-9]+)| ) {
            my $abs_url = "https://arxiv.org/abs/".$1;
            #print "abs_url : $abs_url \n";
            my $abs_cont = HTML::TreeBuilder->new_from_url($abs_url);
            my $license = $abs_cont->look_down( _tag => 'div', 'class' => 'abs-license');
            $license = $license->as_XML;
            #$license = $license->as_text;
            #print "\t license : $license \n";
            if ( $license ) {
                #$license = $license->as_text();
                if ( $license =~ m|creativecommons.org/licenses/by-sa/| ) {
                    print "\tCC-BY-SA article: $abs_url\n";
                }

            }
        }
    }
}
